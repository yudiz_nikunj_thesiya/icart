import React, { Component } from "react";
import Product from "../components/Product";
import { MdShoppingCart } from "react-icons/md";
import pr3 from "../images/iwatches.jpg";
import { Link } from "react-router-dom";

export default class Home extends Component {
	state = {
		cart: 0,
		total: 0,
	};
	getTotalItems = () => {
		const cart = this.state.cart;
		const total = this.state.total;
		this.setState({
			cart: cart + 1,
			total: 55000 * (cart + 1),
		});
	};
	removeItem = () => {
		const cart = this.state.cart;
		const total = this.state.total;
		this.setState({
			cart: cart - 1,
			total: total - 55000,
		});
	};
	render() {
		return (
			<div className="w-full items-center flex flex-col">
				<div className="sticky top-0 flex items-center w-full justify-between bg-purple-700 text-purple-100 shadow-lg px-6 py-4">
					<Link to="/" className="logo font-bold text-3xl">
						iCart
					</Link>
					<div className="flex space-x-4">
						<div
							to="/signup"
							className="px-4 rounded-xl py-4 text-2xl border border-transparent bg-purple-100 text-purple-800 flex items-center space-x-3"
						>
							<MdShoppingCart />
							<span className="text-base">Items {this.state.cart} </span>
							<span className="text-base">Total {`${this.state.total} ₹`}</span>
						</div>
					</div>
				</div>

				<div className="w-2/3 md:w-2/5 lg:w-1/3 py-14">
					<Product
						img={pr3}
						title="Apple Watch Series 7 - Blue Aluminium Case with Abyss Blue Sport Band"
						price="55,000"
						totalItem={this.state.cart}
						increment={this.getTotalItems}
						decrement={this.removeItem}
					/>
				</div>
			</div>
		);
	}
}
