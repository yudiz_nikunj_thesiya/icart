import React, { Component } from "react";

class Product extends Component {
	render() {
		return (
			<div className="flex bg-purple-100 border border-transparent hover:border-purple-200 text-purple-800 flex-col rounded-2xl space-y-4 px-10 py-10">
				<div className="flex flex-col items-center h-52 overflow-hidden rounded-xl">
					<img
						src={this.props.img}
						alt="apple"
						className="w-full h-full object-cover"
					/>
				</div>
				<h3 className="text-lg">{this.props.title}</h3>
				<h3 className="text-2xl font-bold">₹ {this.props.price}</h3>
				<button className="btn" onClick={() => this.props.increment()}>
					Add to Cart
				</button>
				{this.props.totalItem > 0 && (
					<button
						className="btn bg-red-400 border-none"
						onClick={() => this.props.decrement()}
					>
						Remove from Cart
					</button>
				)}
			</div>
		);
	}
}

export default Product;
