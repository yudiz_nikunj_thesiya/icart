import { Routes, Route } from "react-router-dom";
import Home from "./pages/Home";

function App() {
	return (
		<div className="flex flex-col w-full h-screen overflow-y-scroll items-center bg-purple-50">
			<Routes>
				<Route path="/" element={<Home />} />
			</Routes>
		</div>
	);
}

export default App;
